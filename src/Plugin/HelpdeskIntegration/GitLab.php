<?php

namespace Drupal\helpdesk_gitlab\Plugin\HelpdeskIntegration;

use Drupal\Component\Utility\Random;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\comment\CommentInterface;
use Drupal\helpdesk_integration\Entity\Issue;
use Drupal\helpdesk_integration\HelpdeskInterface;
use Drupal\helpdesk_integration\HelpdeskPluginException;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\helpdesk_integration\PluginBase;
use Drupal\user\UserInterface;
use Gitlab\Client;
use Gitlab\Exception\RuntimeException;

/**
 * Plugin implementation of the GitLab.
 *
 * @HelpdeskPlugin(
 *   id = "gitlab",
 *   label = @Translation("GitLab"),
 *   description = @Translation("Provides the GitLab helpdesk plugin.")
 * )
 */
class GitLab extends PluginBase {

  use StringTranslationTrait;

  public const int PER_PAGE = 100;

  private const array RESERVED_USER_NAMES = [
    '.well-known',
    '404.html',
    '422.html',
    '500.html',
    '502.html',
    '503.html',
    'admin',
    'api',
    'apple-touch-icon.png',
    'assets',
    'dashboard',
    'deploy.html',
    'explore',
    'favicon.ico',
    'favicon.png',
    'files',
    'groups',
    'health_check',
    'help',
    'import',
    'jwt',
    'login',
    'oauth',
    'profile',
    'projects',
    'public',
    'robots.txt',
    's',
    'search',
    'sitemap',
    'sitemap.xml',
    'sitemap.xml.gz',
    'slash-command-logo.png',
    'snippets',
    'unsubscribes',
    'uploads',
    'users',
    'v2',
  ];

  /**
   * The GitLab client.
   *
   * @var \Gitlab\Client|null
   */
  private ?Client $adminClient = NULL;

  /**
   * {@inheritdoc}
   */
  public static function settingKeys(): array {
    return [
      'url',
      'api_token',
      'project_id',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(HelpdeskInterface $helpdesk, array $required): array {
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $helpdesk->get('url'),
    ] + $required;
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $helpdesk->get('api_token'),
    ] + $required;
    $form['project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project ID'),
      '#default_value' => $helpdesk->get('project_id'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllIssues(HelpdeskInterface $helpdesk, UserInterface $user, int $since = 0): array {
    if ($since === 0) {
      $since = 1;
    }
    $dt = new \DateTime("@$since");
    $modifiedDate = $dt->format('Y-m-d H:i:s');

    try {
      $stats = $this->getAdminClient($helpdesk)->issuesStatistics()
        ->project($helpdesk->get('project_id'), [
          'updated_after' => $dt,
        ]);
    }
    catch (RuntimeException $ex) {
      return [];
    }
    $count = $stats['statistics']['counts']['all'];
    $loops = (int) ceil($count / self::PER_PAGE);
    $issues = [];
    for ($i = 1; $i <= $loops; $i++) {
      foreach ($this->getAdminClient($helpdesk)->issues()
        ->all($helpdesk->get('project_id'), [
          'per_page' => self::PER_PAGE,
          'page' => $i,
          'updated_after' => $modifiedDate,
        ]) as $incident) {
        $body = empty($incident['description']) ? '' : $incident['description'];
        /** @var \Drupal\helpdesk_integration\IssueInterface $issue */
        $issue = Issue::create([
          'helpdesk' => $helpdesk->id(),
          'extid' => $incident['iid'],
          'resolved' => (string) $incident['state'] === 'closed',
          'title' => $incident['title'],
          'status' => $incident['state'],
          'body' => [
            'value' => $this->filterMarkdownLinks($body),
            'format' => 'basic_html',
          ],
          'created' => strtotime($incident['created_at']),
          'changed' => strtotime($incident['updated_at']),
        ]);
        foreach ($this->getAdminClient($helpdesk)->issues()
          ->showNotes($helpdesk->get('project_id'), $incident['iid']) as $comment) {
          if ($comment['internal']) {
            continue;
          }
          $body = $this->filterMarkdownLinks($comment['body']);
          $issue->addComment(
            $comment['id'],
            $body,
            $user,
            strtotime($comment['created_at']),
            strtotime($comment['updated_at'])
          );
        }
        $issues[] = $issue;
      }
    }

    return $issues;
  }

  /**
   * Filters a link in markdown syntax.
   *
   * @param string $text
   *   The text to filter.
   *
   * @return string
   *   The filtered text.
   */
  private function filterMarkdownLinks(string $text): string {
    return preg_replace_callback('/\[(.*?)]\((.*?)\)/', function () {
      return '';
    }, htmlspecialchars_decode($text));
  }

  /**
   * {@inheritdoc}
   */
  public function createIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    $description = $issue->get('body')->value;
    foreach ($issue->get('attachments')->getValue() as $attachment) {
      $description .= $this->getUploadFileResult($helpdesk, $attachment);
    }
    try {
      $remoteUserId = (int) $this->getRemoteUserId($helpdesk,
        $issue->getOwner());
      $incident = $this->getUserClient($helpdesk, $remoteUserId)->issues()
        ->create($helpdesk->get('project_id'), [
          'issue_type' => 'incident',
          'title' => $issue->getTitle(),
          'description' => $description,
        ]);
      $issue->set('extid', $incident['iid']);
    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addCommentToIssue(HelpdeskInterface $helpdesk, IssueInterface $issue, CommentInterface $comment): void {
    $body = $comment->get('comment_body')->value;
    foreach ($comment->get('field_attachments')->getValue() as $attachment) {
      $body .= $this->getUploadFileResult($helpdesk, $attachment);
    }
    try {
      $remoteUserId = (int) $this->getRemoteUserId($helpdesk,
        $issue->getOwner());
      $newComment = $this->getUserClient($helpdesk, $remoteUserId)->issues()
        ->addNote($helpdesk->get('project_id'),
          (int) $issue->get('extid')->value, $body);
      $comment->set('field_extid', $newComment['id']);
    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * Gets the result of a file upload.
   *
   * @return string
   *   The result.
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function getUploadFileResult(HelpdeskInterface $helpdesk, array $attachment): string {
    try {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->entityTypeManager->getStorage('file')
        ->load($attachment['target_id']);
      $result = $this->getAdminClient($helpdesk)->projects()
        ->uploadFile($helpdesk->get('project_id'), $file->getFileUri());
      return $result['markdown'];
    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resolveIssue(HelpdeskInterface $helpdesk, IssueInterface $issue): void {
    $this->getAdminClient($helpdesk)
      ->issues()
      ->update($helpdesk->get('project_id'),
        (int) $issue->get('extid')->value, [
          'state_event' => 'close',
        ]);
  }

  /**
   * {@inheritdoc}
   */
  public function isUserLocked(HelpdeskInterface $helpdesk, UserInterface $user): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function pushUser(HelpdeskInterface $helpdesk, UserInterface $user): string {
    $email = $user->getEmail();
    $name = $user->getAccountName();
    $username = $user->getAccountName();
    if (in_array($username, self::RESERVED_USER_NAMES)) {
      $username .= '_' . hash('sha256', Settings::getHashSalt());
    }
    try {
      if ($remoteId = $this->getUserData($user, $helpdesk, self::REMOTE_ID)) {
        $this->getAdminClient($helpdesk)->users()
          ->update($remoteId, [
            'email' => $email,
            'name' => $name,
            'username' => $username,
          ]);
      }
      else {
        $remoteUsers = $this->getAdminClient($helpdesk)->users()
          ->all([
            'search' => $email,
          ]);
        if (!empty($remoteUsers)) {
          $this->getAdminClient($helpdesk)->users()
            ->update($remoteUsers[0]['id'], [
              'name' => $name,
              'username' => $username,
            ]);
          $remoteId = $remoteUsers[0]['id'];
        }
        else {
          $userFromPlatform = $this->getAdminClient($helpdesk)->users()
            ->create($email, 'abc', [
              'name' => $name,
              'username' => $username,
              'force_random_password' => TRUE,
              'skip_confirmation' => TRUE,
            ]);
          $remoteId = $userFromPlatform['id'];
        }
      }
      $this->handleMembership($helpdesk, $remoteId);
    }
    catch (\Exception $e) {
      throw new HelpdeskPluginException($e);
    }

    return $remoteId;
  }

  /**
   * Checks membership and adds the given user to the project.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param string $remoteId
   *   The remote user ID.
   */
  private function handleMembership(HelpdeskInterface $helpdesk, string $remoteId): void {
    try {
      $this->getAdminClient($helpdesk)->projects()
        ->allMember($helpdesk->get('project_id'), (int) $remoteId);
    }
    catch (\Exception $e) {
      if ($e->getCode() === 404) {
        $this->getAdminClient($helpdesk)->projects()
          ->addMember($helpdesk->get('project_id'), (int) $remoteId, 20);
      }
    }
  }

  /**
   * Gets the GitLAb client for an admin and initializes if necessary.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   *
   * @return \Gitlab\Client
   *   The GitLab admin client.
   */
  private function getAdminClient(HelpdeskInterface $helpdesk): Client {
    if (!$this->adminClient) {
      $this->adminClient = $this->doCreateClient($helpdesk,
        $helpdesk->get('api_token'));
    }
    return $this->adminClient;
  }

  /**
   * Gets the API Client for a user.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The Helpdesk.
   * @param int $remoteUserId
   *   The email.
   *
   * @return \Gitlab\Client
   *   The GitLab user client.
   */
  private function getUserClient(HelpdeskInterface $helpdesk, int $remoteUserId): Client {
    $token = $this->getUserImpersonationToken($helpdesk, $remoteUserId);
    return $this->doCreateClient($helpdesk, $token);
  }

  /**
   * Creates a client with a specific token.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param string $token
   *   The token.
   *
   * @return \Gitlab\Client
   *   The client.
   */
  private function doCreateClient(HelpdeskInterface $helpdesk, string $token): Client {
    $client = new Client();
    $client->setUrl($helpdesk->get('url'));
    $client->authenticate($token, Client::AUTH_HTTP_TOKEN);
    return $client;
  }

  /**
   * Gets the impersonation token for the given user.
   *
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   *   The helpdesk.
   * @param int $remoteUserId
   *   The user.
   *
   * @return string
   *   The token.
   */
  private function getUserImpersonationToken(HelpdeskInterface $helpdesk, int $remoteUserId): string {
    $tokens = $this->getAdminClient($helpdesk)->users()
      ->userImpersonationTokens($remoteUserId, [
        'state' => 'active',
        'per_page' => self::PER_PAGE,
      ]);

    foreach ($tokens as $token) {
      $this->getAdminClient($helpdesk)->users()
        ->removeImpersonationToken($remoteUserId, $token['id']);
    }

    $expireDate = date('Y-m-d', strtotime('+1 day'));
    $random = new Random();
    $newToken = $this->getAdminClient($helpdesk)->users()
      ->createImpersonationToken($remoteUserId,
        $random->name(), [
          'api',
        ], $expireDate);
    return $newToken['token'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUrl(HelpdeskInterface $helpdesk): ?string {
    try {
      $project = $this->getAdminClient($helpdesk)
        ->projects()
        ->show($helpdesk->get('project_id'));
    }
    catch (RuntimeException $ex) {
      $this->logger->error($ex->getMessage());
      return NULL;
    }
    return $project['web_url'] . '/issues';
  }

}
